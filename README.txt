-------------------------------
Ubercart Product Checkout Panes
-------------------------------

Developed and maintained by Martin B. - martin@webscio.net
Supported by JoyGroup - http://www.joygroup.nl


Introduction
------------
This module allows you to select which panes should be shown at checkout depending on what products are in a user's
cart. It utilizes the powerful Product Features interface to achieve this, meaning that you can now configure
which panes to show during checkout based on product classes and also individually on a per product basis.


Requirements
------------
 * Ubercart Product Checkout Panes requires Ubercart version 2.4 or higher.
 * The patch from http://drupal.org/node/964232.


Installation
------------
 * Copy the module's directory to your modules directory and activate the module.


Usage
-----
 * If there is only one product in your cart and it has the Product Checkout Panes feature enabled, the default
 settings will be ignored and the panes specified for that product will be used instead.

 * If there are multiple products in your cart, the panes displayed will be the union of every product's individual
 pane settings, based on either the Product Checkout Panes feature, if enabled, the default product class settings,
 if specified, or the default Ubercart settings otherwise.
